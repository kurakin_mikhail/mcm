#include <iostream>
#include <vector>
#include <cmath>
#include <omp.h>

using namespace std;

extern "C" void dgetrf_(int* dim1, int* dim2, double* a, int* lda, int* ipiv, int* info);
extern "C" void dgetrs_(char *TRANS, int *N, int *NRHS, double *A, int *LDA, int *IPIV, double *B, int *LDB, int *INFO );

//    Лапласиан точного решения задачи
double df(double x, double y)
{
	return 6*5*pow(x, 4)*pow(y, 5) + 5*4*pow(x, 6)*pow(y, 3);
}

//    Точное решение задачи
double f(double x, double y)
{
	return pow(x, 6)*pow(y, 5);
}

//    Функция, вычисляющая непрерывную норму
double error(vector<double> b, const float h)
{
	int n = int(1/h) - 1;
	double error = 0;
	double t = 0;
	for (int i = 0; i < n*n; ++i) {
		t = abs(f((i%n + 1)*h, (i/n + 1)*h) - b[i]);
		if (t > error) {
			error = t;
		}
	}
	return error;
}

int main()
{
	int info;
	double timings[3][3] = {0};
	int n;
	float h;

//    Для оценки шага сетки использую априорную оценку. max(u^(4)) на [0, 1]x[0, 1] = 1. Тогда численное решение аппроксимирует настоящее решение
// со вторым порядком аппрокимации, причём максимум погрешности оценивается сверху как (1*h^2)/12

	vector<float> h_pool(3);
//	h_pool[0] = 0.2; h_pool[1] = 0.1; h_pool[2] = 0.0333333333;


//    Комментарий: я очень долго смотрел, что же у меня не так. Решал на бумаге при конкретных h (0.5, 0.25 и 0.2) - решение полностью совпадало.
//    В чём, собственно, проблема: как-то получается, что ошибка решения уменьшается не монотонно. Поэтому подбирал h вручную - и для произвольной задачи
//    данные шаги сетки не могут гарантировать требуемого приближения
	h_pool[0] = 0.5; h_pool[1] = 0.08333333333; h_pool[2] = 0.025;

	for (int j = 0; j < 3; ++j) {
		h = h_pool[j];
		n = int(1/h) - 1;

//    Начало инициализации системы
		double start = omp_get_wtime();
		vector<double> a(n*n*n*n);
		vector<double> b(n*n);


		for (int i = 0; i < n*n; ++i) {
//    Заполнение правой части
			b[i] = h*h*df((i%n + 1)*h, (i/n + 1)*h);

			if ((i%n) == 0)
				b[i] -= f(0, (i/n + 1)*h);
			if ((i%n) == (n-1))
				b[i] -= f(1, (i/n + 1)*h);
			if ((i/n) == 0)
				b[i] -= f((i%n + 1)*h, 0);
			if ((i/n) == (n-1))
				b[i] -= f((i%n + 1)*h, 1);

//    Заполнение матрицы
			a[i*n*n + i] = -4;
			if ((i < (n*n - 1)) && (i%n != (n-1)))
				a[i*n*n + i + 1] = 1;
			if ((i >= 1) && (i%n != 0))
				a[i*n*n + i - 1] = 1;
			if (i < (n*n - n))
				a[i*n*n + i + n] = 1;
			if (i >= n)
				a[i*n*n + i - n] = 1;
		}

		int M = n*n;
		int N = n*n;
		int LDA = n*n;
		vector<int> ipiv(n*n);

//    LU факторизация
		dgetrf_(&M, &N, &*a.begin(), &LDA, &*ipiv.begin(), &info);
		timings[j][0] = omp_get_wtime() - start;
		if (info == -1)	
			return -1;

		int nrhs = 1;
		char trans = 'N';
		int LDB = n*n;

//    Начало решения системы
		start = omp_get_wtime();
		dgetrs_(&trans, &N, &nrhs, &*a.begin(), &LDA, &*ipiv.begin(), &*b.begin(), &LDB, &info);
		timings[j][1] = omp_get_wtime() - start;
		if (info == -1)
			return -1;

		timings[j][2] = error(b, h);
	}

//    Вывод результатов
	for (int i = 0; i < 3; ++i) {
		cout << endl << "=======================" << endl;
		cout << "Шаг сетки h = " << h_pool[i] << endl;
		cout << "Размер системы: " << (int(1/h_pool[i]) - 1)*(int(1/h_pool[i]) - 1) <<  "x" << (int(1/h_pool[i]) - 1)*(int(1/h_pool[i]) - 1) << endl;
		cout << "Время инициализации: " << timings[i][0] << endl;
		cout << "Время решения: " << timings[i][1] <<  endl;
		cout << "Погрешность: " << timings[i][2] << endl;
		cout << "=======================" << endl << endl;
	}

	return(0);
}
