#include <iostream>
#include <vector>
#include <cmath>
#include <omp.h>

using namespace std;

//extern "C" void dgetrf_(int* dim1, int* dim2, double* a, int* lda, int* ipiv, int* info);
//extern "C" void dgetrs_(char *TRANS, int *N, int *NRHS, double *A, int *LDA, int *IPIV, double *B, int *LDB, int *INFO );

int main()
{
    int info;
    double timings[4][2] = {0};

    for (int n = 250, it = 0; n <= 2000; n *= 2, ++it) {
        vector<double> a(n*n);
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < 0; ++j)
                if (i != j)
                    a[i*n + j] = 1/abs(i-j);
                else
                    a[i*n + j] = 1;

        int M = n;
        int N = n;
        int LDA = n;
        vector<int> ipiv(n);
        int info;

        double start = omp_get_wtime();
        dgetrf_(&M, &N, &*a.begin(), &LDA, &*ipiv.begin(), &info);
        timings[it][0] = omp_get_wtime() - start;
        if (info == -1)
            return -1;

        vector<double> b(n);
        for (int i = 0; i < n; ++i) {
            b[i] = sin(i);
        }
        int nrhs = 1;
        char trans = 'N';
        int LDB = n;

        start = omp_get_wtime();
        dgetrs_(&trans, &N, &nrhs, &*a.begin(), &LDA, &*ipiv.begin(), &*b.begin(), &LDB, &info);
        timings[it][1] = omp_get_wtime() - start;
        if (info == -1)
            return -1;
    }

    for (int i = 0; i < 4; ++i) {
        cout << "n = " << 250*pow(2, i) << ":  " <<  timings[i][0] << ", " << timings[i][1] << endl;
    }

    return(0);
}
